curl --location 'http://localhost:4318/v1/traces' \

--header 'Content-Type: application/json' \

--data '{

  "resourceSpans": [

    {

      "resource": {

        "attributes": [

          {

            "key": "service.name",

            "value": {

              "stringValue": "my.service"

            }

          }

        ]

      },

      "scopeSpans": [

        {

          "scope": {

            "name": "my.library",

            "version": "1.0.0",

            "attributes": [

              {

                "key": "my.scope.attribute",

                "value": {

                  "stringValue": "some scope attribute"

                }

              }

            ]

          },

          "spans": [

            {

              "traceId": "5B8EFFF798038103D269B633813FC60C",

              "spanId": "EEE19B7EC3C1B174",

              "parentSpanId": "EEE19B7EC3C1B173",

              "name": "I'\''m a server span",

              "startTimeUnixNano": 1697205606000000000,

              "endTimeUnixNano": 1697205608000000000,

              "kind": 2,

              "attributes": [

                {

                  "key": "my.span.attr",

                  "value": {

                    "stringValue": "some value"

                  }

                }

              ]

            }

          ]

        }

      ]

    }

  ]

}'

